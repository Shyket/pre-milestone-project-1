/* eslint-disable no-trailing-spaces */

import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';

export const appRoutes: Route[] = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'posts',
                pathMatch: 'full',
            },

            {
                path: 'sign-in',
                canActivate: [NoAuthGuard],
                loadChildren: () =>
                    import('app/modules/auth/sign-in/sign-in.module').then(
                        (m) => m.AuthSignInModule
                    ),
            },
            {
                path: 'sign-up',
                canActivate: [NoAuthGuard],
                loadChildren: () =>
                    import('app/modules/auth/sign-up/sign-up.module').then(
                        (m) => m.AuthSignUpModule
                    ),
            },
            {
                path: 'sign-out',
                canActivate: [AuthGuard],
                loadChildren: () =>
                    import('app/modules/auth/sign-out/sign-out.module').then(
                        (m) => m.AuthSignOutModule
                    ),
            },
            {
                path: 'profile',
                canActivate: [AuthGuard],
                loadChildren: () =>
                    import('app/modules/profile/profile.module').then(
                        (m) => m.ProfileModule
                    ),
            },

            {
                path: 'posts',
                loadChildren: () =>
                    import('app/modules/post/post.module').then(
                        (m) => m.PostModule
                    ),
            },
        ],
    },
];
