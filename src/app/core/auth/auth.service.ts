/* eslint-disable arrow-body-style */
import { UserService } from 'app/core/user/user.service';
/* eslint-disable arrow-parens */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable no-trailing-spaces */
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, of, switchMap } from 'rxjs';
import { User } from 'app/core/user/user.types';
import CryptoJS from 'crypto-js';

@Injectable()
export class AuthService {
    secret: string = 'why so serious';

    /**
     * logger
     *
     */
    logger: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
        JSON.parse(localStorage.getItem('active'))?.status
    );

    private _authenticated: boolean =
        localStorage.getItem('active') == null
            ? false
            : JSON.parse(localStorage.getItem('active')).status;

    constructor(
        private _httpClient: HttpClient,
        private userService: UserService
    ) {}

    /**
     *signIn
     *
     * @param   credentials object {email:string, password:string}
     * @returns Observable
     */
    signIn(credentials: { email: string; password: string }): Observable<any> {
        return this._httpClient
            .post('https://jsonplaceholder.typicode.com/users', credentials)
            .pipe(
                switchMap((res) => {
                    const observable = new Observable((subscriber) => {
                        const user = JSON.parse(localStorage.getItem('user'));

                        //if there is no user in localstorage then user is not registered yet
                        if (!user) {
                            subscriber.error('Registration first');
                        }

                        if (this._authenticated) {
                            subscriber.error('user already logged in');
                        }

                        //credentials matching with localstorage values.
                        if (
                            user.email === credentials.email &&
                            this.validatePassword(
                                credentials.password,
                                this.secret,
                                user.password
                            )
                        ) {
                            //user is logged in . so active status true
                            localStorage.setItem(
                                'active',
                                JSON.stringify({ status: true })
                            );
                            this._authenticated = true;

                            //successfully logged in. Emoitting true
                            subscriber.next(true);
                            this.logger.next(true);
                        } else {
                            subscriber.next(false);
                        }
                    });

                    return observable;
                })
            );
    }

    /**
     *signOut
     *
     * @returns Observable
     */
    signOut(): Observable<boolean> {
        // Remove the access token from the local storage
        localStorage.setItem('active', JSON.stringify({ status: false }));

        // Set the authenticated flag to false
        this._authenticated = false;
        this.logger.next(false);

        // Return the observable
        return of(true);
    }

    /**
     *
     *@param User
     * @return Observable
     */
    signUp(user: User): Observable<boolean> {
        //hash password before api call
        const newUser: User = {
            ...user,
            password: this.hashPassword(user.password, this.secret),
        };

        return new Observable<boolean>((observer) => {
            this._httpClient
                .post('https://jsonplaceholder.typicode.com/users', newUser)
                .subscribe(
                    (data: User) => {
                        if (data) {
                            //successful, observerer will get true
                            localStorage.setItem('user', JSON.stringify(data));
                            this.userService.updateUser({
                                ...data,
                                avatar: null,
                            });
                            observer.next(true);
                        } else {
                            observer.next(false);
                        }
                    },
                    (error) => {
                        observer.error(error);
                    }
                );
        });
    }

    //
    /**
     * check()
     *
     * returns an behavioural subject whhich emits the logged in status to its observers
     *
     * @returns Observable
     */
    check(): Observable<boolean> {
        return this.logger.asObservable();
    }

    /**
     * hashPassword()
     *
     * @param password string
     * @returns string
     */
    private hashPassword(password: string, secret: string): string {
        return CryptoJS.AES.encrypt(password, secret).toString();
    }

    /**
     * hashPassword()
     *
     * @param password string
     * @returns string
     */
    private validatePassword(
        password: string,
        secret: string,
        hashed: string
    ): boolean {
        return (
            CryptoJS.AES.decrypt(hashed, secret).toString(CryptoJS.enc.Utf8) ===
            password
        );
    }
}
