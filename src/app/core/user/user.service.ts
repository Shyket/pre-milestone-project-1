/* eslint-disable quotes */
/* eslint-disable arrow-body-style */
/* eslint-disable valid-jsdoc */
/* eslint-disable no-trailing-spaces */
/*eslint valid-jsdoc: "error"*/
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable, of, ReplaySubject, tap } from 'rxjs';
import { User } from 'app/core/user/user.types';

@Injectable({
    providedIn: 'root',
})
export class UserService {
    private _user: BehaviorSubject<User> = new BehaviorSubject<User>(
        JSON.parse(localStorage.getItem('user'))
    );

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * @returns Observable<User>
     */
    getCurrentUser(): Observable<User> {
        this._user.next(JSON.parse(localStorage.getItem('user')));
        return this._user.asObservable();
    }

    /**
     * @param updatedUserData User
     * @returns Observable
     */
    updateUser(updatedUserData: User): Observable<any> {
        return this._httpClient
            .put(`https://jsonplaceholder.typicode.com/users/1`, {
                params: updatedUserData,
            })
            .pipe(
                map((res) => {
                    if (res) {
                        const previousUserData = JSON.parse(
                            localStorage.getItem('user')
                        );
                        const newData = {
                            ...previousUserData,
                            ...updatedUserData,
                        };
                        localStorage.setItem('user', JSON.stringify(newData));
                        this._user.next(
                            JSON.parse(localStorage.getItem('user'))
                        );
                    }
                })
            );
    }

    /**
     * @param id number
     * @returns Observable
     */
    getUserNameByID(id: number): Observable<string> {
        //if the id matches with loggedin user id then return the username form localstorage
        if (
            localStorage.getItem('user') &&
            JSON.parse(localStorage.getItem('user')).id === id
        ) {
            return of(JSON.parse(localStorage.getItem('user')).username);
        } else {
            //if the id doesnt match with logged in user then make apicall to get the username
            return this._httpClient
                .get('https://jsonplaceholder.typicode.com/users', {
                    params: { id: id },
                })
                .pipe(
                    map((res: any[]) => {
                        return res[0].username;
                    })
                );
        }
    }
}
