export interface User {
    avatar: File;
    id: number;
    firstname: string;
    lastname: string;
    username: string;
    email: string;
    password: string;
    location: string;
    dialCode: string;
    phoneNumber: number;
    country: string;
}

export interface ExternalUser {
    id: string;
    username: string;
    email: string;
}
