import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlartBoxComponent } from './alart-box.component';

describe('AlartBoxComponent', () => {
  let component: AlartBoxComponent;
  let fixture: ComponentFixture<AlartBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlartBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlartBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
