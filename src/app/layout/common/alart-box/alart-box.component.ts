import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-alart-box',
    templateUrl: './alart-box.component.html',
    styleUrls: ['./alart-box.component.scss'],
})
export class AlartBoxComponent implements OnInit {
    @Input() alartType: string;
    @Input() showAlartMessege: boolean = false;
    @Input() alartMessege: string = '';

    constructor() {}

    ngOnInit(): void {}
}
