/* eslint-disable no-trailing-spaces */
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, Subscription, takeUntil, switchMap, of } from 'rxjs';
import { FuseMediaWatcherService } from '@fuse/services/media-watcher';
import { AuthService } from 'app/core/auth/auth.service';
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {
    isScreenSmall: boolean;
    isLoggedIn: boolean;

    private _unsubscribeAll: Subject<any> = new Subject<any>();
    private subscription: Subscription;

    constructor(
        private _fuseMediaWatcherService: FuseMediaWatcherService,
        private _authService: AuthService
    ) {}

    get currentYear(): number {
        return new Date().getFullYear();
    }

    ngOnInit(): void {
        this._fuseMediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({ matchingAliases }) => {
                // Check if the screen is small
                this.isScreenSmall = !matchingAliases.includes('md');
            });

        this.subscription = this._authService
            .check()
            .subscribe( (isLoggedIn) => {
                this.isLoggedIn = isLoggedIn;
            });
    }

    ngOnDestroy(): void {
        console.log('nav-destroy');

        this.subscription.unsubscribe();
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
