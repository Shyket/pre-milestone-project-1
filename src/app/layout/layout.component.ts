/* eslint-disable no-trailing-spaces */
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LayoutComponent implements OnInit, OnDestroy {
    showNav: boolean = false;
    constructor(private router: Router) {}
    /**
     * @returns number
     */
    get currentYear(): number {
        return new Date().getFullYear();
    }

    ngOnInit(): void {
        //console.log(this.router.url);
    }

    /**
     * @returns void
     */
    onRouteChange(): void{
        if (this.router.url === '/posts' || this.router.url === '/profile') {
            this.showNav = true;
        } else if (
            this.router.url === '/sign-in' ||
            this.router.url === '/sign-up'
        ) {
            this.showNav = false;
        }
    }

    ngOnDestroy(): void {}
}
