/* eslint-disable no-trailing-spaces */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostModule } from './../modules/post/post.module';
import { NgModule } from '@angular/core';
import { LayoutComponent } from 'app/layout/layout.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseLoadingBarModule } from '@fuse/components/loading-bar';
import { RouterModule } from '@angular/router';
import { NavbarModule } from './common/navbar/navbar.module';
import { AlartBoxComponent } from './common/alart-box/alart-box.component';
import { FuseAlertModule } from '@fuse/components/alert';

@NgModule({
    declarations: [LayoutComponent, AlartBoxComponent],
    imports: [
        ReactiveFormsModule,
        FormsModule,
        SharedModule,
        MatMenuModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseLoadingBarModule,
        RouterModule,
        NavbarModule,
        FuseAlertModule,
    ],

    exports: [LayoutComponent, AlartBoxComponent],
})
export class LayoutModule {}
