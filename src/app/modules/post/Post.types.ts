/* eslint-disable no-trailing-spaces */
export interface Post {
    privacy: Privacy;
    userId: number;
    id: number;
    title: string;
    body: string;
}

export enum Privacy {
    private = 'private',
    public = 'public',
}
