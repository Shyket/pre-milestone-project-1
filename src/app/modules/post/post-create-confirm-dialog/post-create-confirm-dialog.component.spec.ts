import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreateConfirmDialogComponent } from './post-create-confirm-dialog.component';

describe('PostCreateConfirmDialogComponent', () => {
  let component: PostCreateConfirmDialogComponent;
  let fixture: ComponentFixture<PostCreateConfirmDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostCreateConfirmDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreateConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
