/* eslint-disable no-trailing-spaces */
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-post-create-confirm-dialog',
    templateUrl: './post-create-confirm-dialog.component.html',
    styleUrls: ['./post-create-confirm-dialog.component.scss'],
})
export class PostCreateConfirmDialogComponent implements OnInit {
    messege: string;
    title: string;
    messegeType: string;
    
    /**
     * @param  {} @Inject(MAT_DIALOG_DATA
     * @param  {} data
     */
    constructor(@Inject(MAT_DIALOG_DATA) data) {
        this.messege = data.messege;
        this.title = data.title;
        this.messegeType = data.messegetype;
    }

    ngOnInit(): void {}
}
