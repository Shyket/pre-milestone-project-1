import { Subscription } from 'rxjs';
import { UserService } from 'app/core/user/user.service';
import { PostService } from './../post.service';
import { Post, Privacy } from './../Post.types';
/* eslint-disable no-trailing-spaces */
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PostCreateConfirmDialogComponent } from '../post-create-confirm-dialog/post-create-confirm-dialog.component';

@Component({
    selector: 'app-post-create',
    templateUrl: './post-create.component.html',
    styleUrls: ['./post-create.component.scss'],
})
export class PostCreateComponent implements OnInit, OnDestroy {
    postCreateForm: FormGroup;
    userID: number;
    userSubscription: Subscription;
    postSubscription: Subscription;
    privacyList: Privacy[] = [Privacy.private, Privacy.public];

    /**
     * @param  _formBuilder FormBuilder
     * @param  dialog MatDialog
     * @param  postService PostService
     * @param  userService UserService
     */
    constructor(
        private _formBuilder: FormBuilder,
        public dialog: MatDialog,
        private postService: PostService,
        private userService: UserService
    ) {}

    ngOnDestroy(): void {
        if (this.userSubscription) {
            this.userSubscription.unsubscribe();
        }
        if (this.postSubscription) {
            this.postSubscription.unsubscribe();
        }
    }

    ngOnInit(): void {
        this.postCreateForm = this._formBuilder.group({
            title: ['', Validators.required],
            body: [''],
            privacy: [Privacy.public],
        });

        this.userSubscription = this.userService
            .getCurrentUser()
            .subscribe((user) => {
                this.userID = user.id;
            });
    }
    /**
     * @param   title string
     * @param   messege string
     * @param   messegeType string
     * @returns void
     */
    openDialog(title: string, messege: string, messegeType: string): void {
        this.dialog.open(PostCreateConfirmDialogComponent, {
            data: {
                messege: messege,
                title: title,
                messegeType: messegeType,
            },
        });
    }

    /**
     * @returns void
     */
    onCreatePost(): void {
        if (this.postCreateForm.invalid) {
            return;
        }

        this.postCreateForm.disable();

        this.postSubscription = this.postService
            .uploadPost({
                id: -100,
                privacy: this.postCreateForm.get('privacy').value,
                title: this.postCreateForm.get('title').value,
                body: this.postCreateForm.get('body').value,
                userId: this.userID,
            })
            .subscribe({
                next: (uploaded) => {
                    this.openDialog(
                        'success',
                        'post created successfully',
                        'success'
                    );
                },
                error: (error) => {
                    console.log(error);
                    this.openDialog('error', 'error occured', 'error');
                },
            });

        this.postCreateForm.enable();
    }
}
