/* eslint-disable arrow-parens */
import { Post } from './../Post.types';
/* eslint-disable no-trailing-spaces */
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'app/core/user/user.service';
import { PostService } from '../post.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PostDeleteConfirmComponent } from '../post-delete-confirm/post-delete-confirm.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-post-details',
    templateUrl: './post-details.component.html',
    styleUrls: ['./post-details.component.scss'],
})
export class PostDetailsComponent implements OnInit, OnDestroy {
    paramPostID: number;
    post: Post;
    postOwnerName: string;
    editSectionVisibility: boolean;
    private postGetSusbscription: Subscription;
    private postDeleteSusbscription: Subscription;
    private usernameSusbscription: Subscription;
    /**
     * @param  router Router
     * @param  userService UserService
     * @param  postService PostService
     * @param  _snackBar MatSnackBar
     */
    constructor(
        private router: Router,
        private userService: UserService,
        private postService: PostService,
        private _snackBar: MatSnackBar
    ) {
        if (this.router.getCurrentNavigation()) {
            this.paramPostID = Number(
                this.router.getCurrentNavigation().extractedUrl.root.children
                    .primary.segments[1].path
            );
        }
    }

    ngOnInit(): void {
        this.postGetSusbscription = this.postService
            .getPostByID(this.paramPostID)
            .subscribe({
                next: (data: Post) => {
                    this.post = data;

                    this.getPosterName();
                    if (
                        JSON.parse(localStorage.getItem('user')).id ===
                        this.post?.userId
                    ) {
                        this.editSectionVisibility = true;
                    }
                },
                error: (error) => {
                    console.log(error);
                },
            });
    }
    /**
     * @returns void
     */
    openSnackBar(): void {
        this._snackBar.openFromComponent(PostDeleteConfirmComponent, {
            duration: 5 * 1000,
        });
    }

    /**
     * @returns void
     */
    getPosterName(): void {
        this.usernameSusbscription = this.userService
            .getUserNameByID(this.post.userId)
            .subscribe({
                next: (name: string) => {
                    this.postOwnerName = name;
                },
                error: (error) => {
                    console.log(error);
                },
            });
    }
    /**
     * @returns void
     */
    ngOnDestroy(): void {
        this.unsubscribe(this.postDeleteSusbscription);
        this.unsubscribe(this.usernameSusbscription);
        this.unsubscribe(this.postGetSusbscription);
    }
    /**
     * @returns void
     */
    onDeletePost(): void {
        this.postDeleteSusbscription = this.postService
            .deletePost(this.post.id)
            .subscribe({
                next: (deleted) => {
                    this.openSnackBar();
                    this.router.navigate(['./posts']);
                },
                error: (error) => {
                    console.log(error);
                },
            });
    }

    private unsubscribe(subscription: Subscription): void {
        if (subscription) {
            subscription.unsubscribe();
        }
    }
}
