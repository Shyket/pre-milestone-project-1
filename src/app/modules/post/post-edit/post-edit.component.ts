/* eslint-disable arrow-parens */
/* eslint-disable no-trailing-spaces */
import { Subscription } from 'rxjs';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostService } from '../post.service';
import { Post, Privacy } from '../Post.types';
import { TransitionCheckState } from '@angular/material/checkbox';

@Component({
    selector: 'app-post-edit',
    templateUrl: './post-edit.component.html',
    styleUrls: ['./post-edit.component.scss'],
})
export class PostEditComponent implements OnInit, OnDestroy {
    @Input() post: Post;

    postUpdateSubscription: Subscription;
    alartType: string;
    showAlartMessege: boolean = false;
    alartMessege: string = '';
    postEditForm: FormGroup;
    panelOpenState = false;
    privacyList: Privacy[] = [Privacy.private, Privacy.public];

    /**
     * @param  formBuilder FormBuilder
     * @param  postService PostService
     */
    constructor(
        private formBuilder: FormBuilder,
        private postService: PostService
    ) {}

    ngOnInit(): void {
        this.postEditForm = this.formBuilder.group({
            title: [this.post.title, Validators.required],
            body: [this.post.body, Validators.required],
            privacy: [this.post.privacy],
        });
    }

    /**
     * @returns void
     */
    onUpdateButtonClick(): void {
        this.postEditForm.disable();
        const newData = this.postEditForm.value;
        const updatedPost: Post = { ...this.post, ...newData };
        this.postUpdateSubscription = this.postService
            .updatePost(updatedPost)
            .subscribe({
                next: (res) => {
                    this.postEditForm.enable();

                    this.setAlart('info', 'Post Updated Successfully');
                },
                error: (error) => {
                    this.setAlart('error', 'Error occured');
                },
            });
    }

    ngOnDestroy(): void {
        this.unsubscribe(this.postUpdateSubscription);
    }

    private setAlart(alartType: string, alaertmessege: string): void {
        this.showAlartMessege = true;
        this.alartType = alartType;
        this.alartMessege = alaertmessege;
    }

    private unsubscribe(subscription: Subscription): void {
        if (subscription) {
            subscription.unsubscribe();
        }
    }
}
