/* eslint-disable arrow-parens */
/* eslint-disable no-trailing-spaces */
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PostService } from '../post.service';
import { Post } from '../Post.types';

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit, OnDestroy {
    errorMessege: string;
    postList: Post[];
    private postSubscription: Subscription;

    /**
     * @param postService PostService
     */
    constructor(private postService: PostService) {}

    ngOnInit(): void {
        this.postSubscription = this.postService.getPostList().subscribe({
            next: (data) => {
                this.postList = data;
            },
            error: (error) => {
                this.errorMessege = 'error fetching data';
            },
        });
    }

    ngOnDestroy(): void {
        if (this.postSubscription) {
            this.postSubscription.unsubscribe();
        }
    }
}
