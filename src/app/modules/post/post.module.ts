import { LayoutModule } from './../../layout/layout.module';
import { MatOptionModule } from '@angular/material/core';
/* eslint-disable no-trailing-spaces */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { PostRoutingModule } from './post.routing.module';
/* eslint-disable @typescript-eslint/naming-convention */
import { NgModule } from '@angular/core';
import { ExtraOptions, PreloadAllModules, RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@fuse/components/card';
import { FuseAlertModule } from '@fuse/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { PostListComponent } from './post-list/post-list.component';
import { PostComponent } from './post/post.component';
import { PostService } from './post.service';
import { PostDetailsComponent } from './post-details/post-details.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PostCreateConfirmDialogComponent } from './post-create-confirm-dialog/post-create-confirm-dialog.component';
import { PostEditComponent } from './post-edit/post-edit.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PostDeleteConfirmComponent } from './post-delete-confirm/post-delete-confirm.component';



const routerConfig: ExtraOptions = {
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
};

@NgModule({
    declarations: [
        PostListComponent,
        PostComponent,
        PostDetailsComponent,
        PostCreateComponent,
        PostCreateConfirmDialogComponent,
        PostEditComponent,
        PostDeleteConfirmComponent,
    ],
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        PostRoutingModule,
        MatMenuModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule,
        MatDialogModule,
        MatExpansionModule,
        MatOptionModule,
        MatSelectModule,
        MatSnackBarModule,
        LayoutModule
    ],
    exports: [PostListComponent, PostComponent],
    providers: [PostService],
})
export class PostModule {}
