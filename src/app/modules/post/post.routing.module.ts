import { PostCreateComponent } from './post-create/post-create.component';
/* eslint-disable no-trailing-spaces */
import { PostDetailsComponent } from './post-details/post-details.component';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';

export const postRoutes: Route[] = [
    {
        path: '',
        component: PostListComponent,
    },

    {
        path: 'create',
        canActivate: [AuthGuard],
        component: PostCreateComponent,
    },
    {
        path: ':id',
        canActivate: [AuthGuard],
        component: PostDetailsComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(postRoutes)],
    exports: [RouterModule],
})
export class PostRoutingModule {}
