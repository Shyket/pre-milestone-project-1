import { ReplaySubject, BehaviorSubject, switchMap } from 'rxjs';
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable arrow-parens */
/* eslint-disable no-trailing-spaces */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Post, Privacy } from './Post.types';

@Injectable()
export class PostService {
    private postList: BehaviorSubject<Post[]> = new BehaviorSubject<Post[]>(
        JSON.parse(localStorage.getItem('posts'))
    );

    private currentPost: BehaviorSubject<Post> = new BehaviorSubject<Post>(
        null
    );

    /**
     * @constructor
     * @param httpClient HttpClient
     */
    constructor(private httpClient: HttpClient) {}

    /**
     * @returns Observable
     */
    getPostList(): Observable<any> {
        if (localStorage.getItem('posts')) {
            this.postList.next(JSON.parse(localStorage.getItem('posts')));
            return this.postList.asObservable();
        } else {
            return this.httpClient
                .get('https://jsonplaceholder.typicode.com/posts')
                .pipe(
                    switchMap((data: Post[]) => {
                        //modify the reault array by adding privacy field
                        const modifiedData: Post[] = data.slice(0, 10);
                        modifiedData.forEach(
                            (n) => (n.privacy = Privacy.public)
                        );
                        localStorage.setItem(
                            'posts',
                            JSON.stringify(modifiedData)
                        );
                        this.postList.next(modifiedData);
                        return this.postList;
                    })
                );
        }
    }

    /**
     * @param  id number
     * @returns Observable
     */
    getPostByID(id: number): Observable<any> {
        if (localStorage.getItem('posts')) {
            const postList: Post[] = JSON.parse(localStorage.getItem('posts'));

            const post: Post[] = postList.filter((p) => p.id === id);

            if (post.length > 0) {
                this.currentPost.next(post[0]);
                return this.currentPost.asObservable();
            }
        }
        return this.httpClient
            .get('https://jsonplaceholder.typicode.com/posts', {
                params: { id: id },
            })
            .pipe(
                switchMap((res) => {
                    const modifiedData: Post = res[0];
                    modifiedData.privacy = Privacy.public;
                    this.currentPost.next(modifiedData);
                    return this.currentPost;
                })
            );
    }

    /**
     * @param post Post
     * @returns Observable
     */
    updatePost(post: Post): Observable<any> {
        return this.httpClient
            .put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
                params: {
                    id: post.id,
                    title: post.title,
                    body: post.body,
                    userId: post.userId,
                },
            })
            .pipe(
                map((res) => {
                    //find the post with id and update it in the localstorage
                    const postList = JSON.parse(localStorage.getItem('posts'));
                    const postID = postList.findIndex(
                        (el) => el.id === post.id
                    );
                    postList[postID] = post;
                    localStorage.setItem('posts', JSON.stringify(postList));
                    this.currentPost.next(post);
                })
            );
    }

    /**
     * @param post Post
     * @returns Observable
     */
    uploadPost(post: Post): Observable<any> {
        return this.httpClient
            .post('https://jsonplaceholder.typicode.com/posts', {
                params: { ...post },
            })
            .pipe(
                map((res) => {
                    let posts: Post[] = JSON.parse(
                        localStorage.getItem('posts')
                    );
                    if (posts) {
                        post.id = posts.length + 1;
                        posts.push(post);
                    } else {
                        //if the local storage posts field dont exist then create an empty Post array and push the post into it
                        post.id = 1;
                        posts = [];
                        posts.push(post);
                    }

                    localStorage.setItem('posts', JSON.stringify(posts));
                    this.postList.next(
                        JSON.parse(localStorage.getItem('posts'))
                    );
                })
            );
    }

    /**
     * @param  postID number
     * @returns Observable
     */
    deletePost(postID: number): Observable<any> {
        return this.httpClient
            .delete(`https://jsonplaceholder.typicode.com/posts/${postID}`)
            .pipe(
                map((res) => {
                    const posts: Post[] = JSON.parse(
                        localStorage.getItem('posts')
                    );
                    if (posts) {
                        const filteredPosts = posts.filter(
                            (post) => post.id !== postID
                        );
                        localStorage.setItem(
                            'posts',
                            JSON.stringify(filteredPosts)
                        );
                        this.postList.next(
                            JSON.parse(localStorage.getItem('posts'))
                        );
                    }
                })
            );
    }
}
