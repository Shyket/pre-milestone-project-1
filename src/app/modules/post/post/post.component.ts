/* eslint-disable no-trailing-spaces */
import { UserService } from 'app/core/user/user.service';
import { Post, Privacy } from './../Post.types';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ExternalUser, User } from 'app/core/user/user.types';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit, OnDestroy {
    @Input() post: Post;
    username: string;
    usernameSubscription: Subscription;
    authSubscription: Subscription;
    isPrivate: boolean;
    isloggedIn: boolean;

    /**
     * @param  userService UserService
     * @param  authService AuthService
     * @param  router Router
     */
    constructor(
        private userService: UserService,
        private authService: AuthService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.isPrivate = this.post.privacy === Privacy.private ? true : false;
        this.usernameSubscription = this.userService
            .getUserNameByID(this.post.userId)
            .subscribe(
                (name: string) => {
                    this.username = name;
                },
                (error) => {
                    console.log(error);
                }
            );
        this.authSubscription = this.authService
            .check()
            .subscribe((isLoggedIn) => {
                this.isloggedIn = isLoggedIn;
            });
    }

    ngOnDestroy(): void {
        if (this.authSubscription) {
            this.authSubscription.unsubscribe();
        }
        if (this.usernameSubscription) {
            this.usernameSubscription.unsubscribe();
        }
    }
    /**
     * @returns void
     */
    onPostClick(): void {
        this.router.navigate(['./posts', this.post.id], {
            state: { postID: this.post.id },
        });
    }
}
