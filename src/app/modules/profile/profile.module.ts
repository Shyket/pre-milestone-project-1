import { LayoutModule } from './../../layout/layout.module';
/* eslint-disable no-trailing-spaces */
import { FuseAlertModule } from './../../../@fuse/components/alert/alert.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { MatCommonModule, MatOptionModule, MatOptionSelectionChange } from '@angular/material/core';
import { SharedModule } from 'app/shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { FuseCardModule } from '@fuse/components/card';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinner, MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
    declarations: [ProfileComponent],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatCommonModule,
        SharedModule,
        MatIconModule,
        FuseAlertModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatOptionModule,
        MatCommonModule,
        MatSelectModule,
        LayoutModule
    ],
})
export class ProfileModule {}
