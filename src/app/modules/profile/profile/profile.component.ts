/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
import { UserService } from 'app/core/user/user.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { User } from 'app/core/user/user.types';
import { Subscription } from 'rxjs';
import { countries } from './country.const';
@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {

    showAlartMessege: boolean = false;
    alartMessege: string;
    alartType: string;
    avatarURL: any = null;
    profileFormGroup: FormGroup;
    currentUser: User;
    countries = countries;
    private userFetchsubscription: Subscription;
    private userUpdateSubscription: Subscription;

    /**
     * @param FormBuilder FormBuilder
     * @param userService UserService
     */
    constructor(
        private FormBuilder: FormBuilder,
        private userService: UserService
    ) {}

    ngOnInit(): void {
        this.makeUserFetchSubscription();
    }

    ngOnDestroy(): void {
        this.unsubscribe(this.userFetchsubscription);
        this.unsubscribe(this.userUpdateSubscription);
    }

    /**
     * @returns void
     */
    onUpdateUsername(): void {
        this.profileFormGroup
            .get('username')
            .setValue(
                `${this.profileFormGroup.get('firstname').value} ${
                    this.profileFormGroup.get('lastname').value
                }`
            );

        this.profileFormGroup.get('username').markAsTouched({ onlySelf: true });
    }

    /**
     * @param  {} event
     * @returns void
     */
    onAvatarInputChange(event): void {
        const file: File = event.target.files[0];
        const fileReader = new FileReader();

        if (file) {
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                this.avatarURL = fileReader.result;
                this.profileFormGroup.get('avatar').setValue(fileReader.result);
            };
        }
    }

    /**
     * @returns void
     */
    onUpdateButtonClick(): void {
        this.profileFormGroup.disable();
        const updatedUserData = this.profileFormGroup.value;

        this.userUpdateSubscription = this.userService
            .updateUser(updatedUserData)
            .subscribe({
                next: (updated) => {
                    this.profileFormGroup.enable();
                    this.setAlart('Profile Updated Successfully', 'info');
                },
                error: (error) => {
                    this.profileFormGroup.enable();
                    this.setAlart('Unexpected Error Occured', 'error');
                },
                complete: () => {
                    this.userUpdateSubscription.unsubscribe();
                },
            });
    }
    /**
     * @param user User
     */
    private createFormGroup(user: User) {
        if (user) {
            this.profileFormGroup = this.FormBuilder.group({
                firstname: [user.firstname, Validators.required],
                lastname: [user.lastname, Validators.required],
                username: [user.username, Validators.maxLength(15)],
                email: [user.email],
                location: [user.location],
                dialCode: [user.dialCode],
                phoneNumber: [
                    user.phoneNumber,
                    [Validators.maxLength(10), Validators.minLength(10)],
                ],
                country: [user.country],
                avatar: [user.avatar],
            });
        }
    }

    /**
     * @param alartMessege string
     * @param alartType string
     * @returns void
     */
    private setAlart(alartMessege: string, alartType: string): void {
        this.showAlartMessege = true;
        this.alartMessege = alartMessege;
        this.alartType = alartType;
    }

    private makeUserFetchSubscription() {
        this.userFetchsubscription = this.userService
            .getCurrentUser()
            .subscribe((user) => {
                this.currentUser = user;
                this.createFormGroup(this.currentUser);
                this.avatarURL = this.currentUser?.avatar;
            });
    }

    /**
     * @param subscription Subscription
     * @returns void
     */
    private unsubscribe(subscription: Subscription): void {
        if (subscription) {
            subscription.unsubscribe();
        }
    }
}
